from django.contrib import admin

from students.models import Student, Message

admin.site.register(Message)
admin.site.register(Student)
