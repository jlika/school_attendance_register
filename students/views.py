from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView, CreateView, UpdateView, DeleteView, DetailView
# from django.views.decorators.csrf import csrf_exempt
# from rest_framework.parsers import JSONParser
# from students.models import Message
from students.serializers import MessageSerializer, UserSerializer, GroupSerializer
from students.forms import StudentCreationForm, ProfileCreationForm
from students.models import Student, Profile
from rest_framework import generics, permissions
from .models import Message, User, Group


class SignupView(TemplateView):
    template_name = "registration/signup.html"


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            form.save()
            return redirect('home.html')
        else:
            form = UserCreationForm()
            return render(request, 'registration/signup.html', {'form': form})
    else:

        return HttpResponseRedirect('/')


class StudentListView(TemplateView):
    model = Student
    template_name = "student.html"

    def get_context_data(self, **kwargs):
        context = super(StudentListView, self).get_context_data(**kwargs)
        context['students'] = Student.objects.filter(user_id=self.request.user.id)
        return context


class StudentCreateView(CreateView):
    template_name = "student/form.html"
    model = Student
    form_class = StudentCreationForm
    success_url = reverse_lazy('student:class')

    def get_context_data(self, **kwargs):
        context = super(StudentCreateView, self).get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user_id = self.request.user.id
        self.object.save()
        return HttpResponseRedirect(self.success_url)

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))


class StudentUpdateView(UpdateView):
    template_name = "student/form.html"
    model = Student
    form_class = StudentCreationForm
    success_url = reverse_lazy('student:class')

    def get_context_data(self, **kwargs):
        context = super(StudentUpdateView, self).get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        self.object = form.save()
        return HttpResponseRedirect(self.success_url)

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))


class StudentDeleteView(DeleteView):
    model = Student
    success_url = reverse_lazy('student:class')

    def get(self, request, *args, **kwargs):
        self.get_object().delete()
        messages.success(request, 'Student is removed from the Class Successfully!')
        return HttpResponseRedirect(self.success_url)

    def delete(self, request, *args, **kwargs):
        self.get_object().delete()
        return HttpResponseRedirect(self.success_url)


def search(request):
    value = request.GET['search_value']
    students = Student.objects.filter(first_name__contains=value, user_id=request.user.id)
    results = []
    for student in students:
        results.append({
            "id": student.id,
            "image": student.image,
            "student_name": student.student_name,
            "student_id": student.student_id,
            "subject": student.subject,
            "date": student.date,
            "class_time": student.class_time,
            "teacher": student.teacher,
            "class": student.class_number,
            "status": student.status,
        })

    return JsonResponse({
        'data': results
    })


class StudentDetailView(DetailView):
    model = Profile
    template_name = "student/student_card.html"

    def get_context_data(self, **kwargs):
        context = super(StudentDetailView, self).get_context_data(**kwargs)
        context['profiles'] = Student.objects.filter(user_id=self.request.user.id)
        return context


class StudentProfileUpdateView(UpdateView):
    template_name = "student/student_details.html"
    model = Student
    form_class = ProfileCreationForm
    success_url = reverse_lazy('student:class')

    def get_context_data(self, **kwargs):
        context = super(StudentProfileUpdateView, self).get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        self.object = form.save()
        return HttpResponseRedirect(self.success_url)

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))


class MessageList(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)

    queryset = Message.objects.all()
    Serializer_class = MessageSerializer

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class MessageDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticated,)

    queryset = Message.objects.all()
    serializer_class = MessageSerializer


class Users(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetails(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class GroupList(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)

    queryset = Group.objects.all()
    serializer_class = GroupSerializer

