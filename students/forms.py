from django import forms
from django.contrib.auth.models import User
from students.models import Student, Profile


class StudentCreationForm(forms.ModelForm):
    class Meta:
        model = Student
        exclude = ['user']
        widgets = {
            'image': forms.ClearableFileInput(),
            'student_name': forms.TextInput(attrs={'class': 'w3-input w3-border'}),
            'student_id': forms.TextInput(attrs={'class': 'w3-input w3-border'}),
            'subject': forms.TextInput(attrs={'class': 'w3-input w3-border'}),
            'date': forms.DateInput(attrs={'class': 'w3-input w3-border'}),
            'class_time': forms.TimeInput(attrs={'class': 'w3-input w3-border'}),
            'teacher': forms.TextInput(attrs={'class': 'w3-input w3-border'}),
            'class_number': forms.TextInput(attrs={'class': 'w3-input w3-border'}),
            'status': forms.TextInput(attrs={'class': 'w3-input w3-border'}),
        }


class ProfileCreationForm(forms.ModelForm):
    class Meta:
        model = Profile
        exclude = ['user']
        widgets = {
            'image': forms.ClearableFileInput(),
            'student_name': forms.TextInput(attrs={'class': 'w3-input w3-border'}),
            'student_id': forms.TextInput(attrs={'class': 'w3-input w3-border'}),
            'father_name': forms.TextInput(attrs={'class': 'w3-input w3-border'}),
            'birthday': forms.TextInput(attrs={'class': 'w3-input w3-border'}),
            'birthplace': forms.TextInput(attrs={'class': 'w3-input w3-border'}),
            'phone': forms.TextInput(attrs={'class': 'w3-input w3-border'}),
            'email': forms.TextInput(attrs={'class': 'w3-input w3-border'}),
            'address': forms.TextInput(attrs={'class': 'w3-input w3-border'}),
            'class_number': forms.TextInput(attrs={'class': 'w3-input w3-border'}),
        }

        class UserRegistrationForm(forms.Form):
            email = forms.EmailField(max_length=200, help_text='Required. Inform a valid email address.'),

            class Meta:
                model = User
                fields = ('first_name', 'last_name', 'teacher_id', 'subject', 'class', 'email', 'password1', 'password2',)
