from django.contrib.auth.models import User
from django.db import models


class Student(models.Model):
    class Meta:
        verbose_name = 'Studenti'
        verbose_name_plural = 'Studentet'
        db_table = 'student'

    image = models.ImageField(max_length=None, blank=True)
    student_name = models.CharField('Emri Studentit', max_length=20)
    student_id = models.CharField('ID Studentit', max_length=4)
    subject = models.CharField('Lenda', max_length=20)
    date = models.CharField('Data', max_length=20, blank=True)
    class_time = models.CharField('Ora', max_length=20, blank=True)
    teacher = models.CharField('Mesuesi', max_length=20, blank=True)
    class_number = models.CharField('Klasa', max_length=2, blank=True)
    status = models.CharField('Statusi', max_length=20)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return '{} {}'.format(self.student_name, self.student_id)


class Profile(models.Model):
    class Meta:
        verbose_name = 'Profile'
        verbose_name_plural = 'Profiles'
        db_table = 'profile'

    image = models.ImageField(max_length=None, blank=True)
    student_name = models.CharField('Emri Studentit', max_length=20)
    student_id = models.CharField('ID Studentit', max_length=4)
    father_name = models.CharField('Atesia', max_length=20, blank=True)
    birthday = models.CharField('Datelindja', max_length=20, blank=True)
    birthplace = models.CharField('Vendlindja', max_length=20, blank=True)
    phone = models.CharField('Nr. Telefonit', max_length=20, blank=True)
    email = models.EmailField('Email', max_length=20, blank=True)
    address = models.CharField('Adresa', max_length=20, blank=True)
    class_number = models.CharField('Klasa', max_length=2, blank=True)

    def __str__(self):
        return '{} {}'.format(self.student_name, self.student_id)


class Message(models.Model):
    message = models.CharField(max_length=100)
    time = models.TimeField(auto_now=True)
    owner = models.ForeignKey('auth.User', related_name='Message', on_delete=models.CASCADE)

    class Meta:
        ordering = ('time',)


class User(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=15)
    statusMessage = models.CharField(max_length=200)
    imageURL = models.URLField()
    message = models.ForeignKey(Message, on_delete=models.CASCADE)  # One to Many relation between User and Messages
    group = models.ManyToManyField("Group")  # Many to Many relations between User and group

    class Meta:
        ordering = ('name',)


class Group(models.Model):
    name = models.CharField(max_length=200)
    imageURL = models.URLField(max_length=200)
    users = models.ManyToManyField("User", related_name="users")

    class Meta:
        ordering = ('name',)


class Details(models.Model):
    class Meta:
        verbose_name = 'Detail'
        verbose_name_plural = 'Details'

    image = models.ImageField(max_length=None, blank=True)
    student_name = models.CharField('Emri Studentit', max_length=20)
    student_id = models.CharField('ID Studentit', max_length=4)
    class_nummber = models.CharField('Klasa', max_length=2)
