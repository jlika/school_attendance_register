from django.contrib.auth.decorators import login_required
from django.urls import path, include

from students.views import StudentListView, StudentCreateView, StudentUpdateView, StudentDeleteView, search, StudentProfileUpdateView, StudentDetailView

student_urlpatterns = ([
                           path('class/', login_required(StudentListView.as_view()), name="class"),
                           path('add/', login_required(StudentCreateView.as_view()), name="add"),
                           path('update/<int:pk>/', login_required(StudentUpdateView.as_view()), name="update"),
                           path('delete/<int:pk>/', login_required(StudentDeleteView.as_view()), name="delete"),
                           path('search/', login_required(search), name="search"),
                           path('card/', login_required(StudentDetailView), name="card"),
                           path('chat/', login_required(), name="chat"),
                           path('details/update/<int:pk>/', login_required(StudentProfileUpdateView.as_view()), name="profile-update"),
                           # # URL form : "/api/messages/1/2"
                           # path('api/messages/<int:sender>/<int:receiver>', views.message_list, name='message-detail'),  # For GET request.
                           # # URL form : "/api/messages/"
                           # path('api/messages/', views.message_list, name='message-list'),  # For POST
                           # # URL form "/api/users/1"
                           # path('api/users/<int:pk>', views.user_list, name='user-detail'),  # GET request for user with id
                           # path('api/users/', views.user_list, name='user-list'),

                       ],
                       'student')

urlpatterns = [
    path('student/', include(student_urlpatterns)),

]
