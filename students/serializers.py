from django.contrib.auth.models import User
from rest_framework import serializers
from students.models import Message, Group


#
# User Serializer
# class UserSerializer(serializers.ModelSerializer):
#     """For Serializing User"""
#     password = serializers.CharField(write_only=True)
#
#     class Meta:
#         model = User
#         fields = ['username', 'password']
#
#
# #
# # Message Serializer
# class MessageSerializer(serializers.ModelSerializer):
#     """For Serializing Message"""
#     sender = serializers.SlugRelatedField(many=False, slug_field='username', queryset=User.objects.all())
#     receiver = serializers.SlugRelatedField(many=False, slug_field='username', queryset=User.objects.all())
#
#     class Meta:
#         model = Message
#         fields = ['sender', 'receiver', 'message', 'timestamp']


class MessageSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = Message
        fields = ('id', 'message', 'time', 'owner')


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('id', 'name', 'imageURL')


class UserSerializer(serializers.ModelSerializer):
    messages = serializers.PrimaryKeyRelatedField(many=True, queryset=Message.objects.all())

    class Meta:
        model = User
        fields = ('id', 'username', 'name', 'number', 'statusMessage', 'imageURL', 'messages')


class GroupSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    users = serializers.PrimaryKeyRelatedField(many=True, queryset=User.objects.all())

    class Meta:
        model = Group
        fields = ('id', 'name', 'imageURL', 'users', 'owner')
