from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.urls import path, include
from django.views.generic import TemplateView

from django.conf import settings

import students.urls
from students.views import signup, SignupView

urlpatterns = [
                  path('admin/', admin.site.urls),
                  path('', include('django.contrib.auth.urls')),
                  path('', login_required(TemplateView.as_view(template_name='home.html')), name='home'),
                  path('signup/', SignupView.as_view(), name='signup'),
                  path('signup/save/', signup, name='signup-save'),

              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += students.urls.urlpatterns
